import React from 'react';
import ReactDOM from 'react-dom';
import Accueil from './Modules/Accueil';
import './style.css'; 

ReactDOM.render(
  <React.StrictMode>
    <Accueil/>
  </React.StrictMode>,
  document.getElementById('root')
);
