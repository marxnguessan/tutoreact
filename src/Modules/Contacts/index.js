import React, { useState } from 'react';
import ProductCard from '../../Components/ProductCard';

const DATAS = [
    {
        id: 0,
        title: 'Produit 1',
        description : 'Produit ',
        price: 1245.36,
        image : 'https://api.tims-group.com/v1/Uploads/Products/1/617e31649e123.jpeg'
    },
    {
        id: 1,
        title: 'Produit 2',
        description : 'Produit ',
        price: 1245.36,
        image : 'https://api.tims-group.com/v1/Uploads/Products/2/617e31aef3935.jpeg'
    },
    {
        id: 2,
        title: 'Produit 3',
        description : 'Produit ',
        price: 1245.36,
        image : 'https://api.tims-group.com/v1/Uploads/Products/3/617e3e9e51aa0.jpeg'
    },
    {
        id: 3,
        title: 'Produit 4',
        description : 'Produit ',
        price: 1245.36,
        image : 'https://api.tims-group.com/v1/Uploads/Products/4/617e3efb9ea79.jpeg'
    },
    {
        id: 4,
        title: 'Produit 5',
        description : 'Produit ',
        price: 1245.36,
        image : 'https://api.tims-group.com/v1/Uploads/Products/5/617e3f7c79436.jpeg'
    },
    {
        id: 0,
        title: 'Produit 1',
        description : 'Produit ',
        price: 1245.36,
        image : 'https://api.tims-group.com/v1/Uploads/Products/1/617e31649e123.jpeg'
    },
    {
        id: 1,
        title: 'Produit 2',
        description : 'Produit ',
        price: 1245.36,
        image : 'https://api.tims-group.com/v1/Uploads/Products/2/617e31aef3935.jpeg'
    },
    {
        id: 2,
        title: 'Produit 3',
        description : 'Produit ',
        price: 1245.36,
        image : 'https://api.tims-group.com/v1/Uploads/Products/3/617e3e9e51aa0.jpeg'
    },
    {
        id: 3,
        title: 'Produit 4',
        description : 'Produit ',
        price: 1245.36,
        image : 'https://api.tims-group.com/v1/Uploads/Products/4/617e3efb9ea79.jpeg'
    },
    {
        id: 4,
        title: 'Produit 5',
        description : 'Produit ',
        price: 1245.36,
        image : 'https://api.tims-group.com/v1/Uploads/Products/5/617e3f7c79436.jpeg'
    }
]; 
const Contacts = () => {

    const [itemSelected, setItemSelected] = useState(null)
    return <>

        <h3> Les produits</h3>


        {itemSelected && <div className="detailsImage">
                        <div className="content">
                            <div className="imagePic">
                                <img src={itemSelected.image} alt="" width = "100" height = "100" />
                            </div>

                            <h5> {itemSelected.title} </h5>

                            <p> {itemSelected.description}</p>

                            <h6> {itemSelected.price}</h6>
                        </div>    
        </div>}

        <div className="productItems">

                    {DATAS.map((item, index) => {
                        return (
                            <ProductCard
                            image = {item.image}
                            title = {item.title}
                            description = {item.description}
                            price = {item.price}
                            setItemSelected = {setItemSelected}
                            />
                        )
                    })

                    }
                

        </div>
    </>
}


export default Contacts;