import React from 'react'

const ProductCard = (props) => {

    const {image, title, description, price, setItemSelected} = props
    return (
            <div className="product"

            onClick = {() => setItemSelected(props)}
            >
                <div className="content">
                    <div className="imagePic">
                        <img src={image} alt="" width = "100" height = "100" />
                    </div>

                    <h5> {title} </h5>

                    <p> {description}</p>

                    <h6> {price}</h6>
                </div>
            </div>
    )
}

export default ProductCard 
